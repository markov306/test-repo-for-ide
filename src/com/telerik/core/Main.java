package com.telerik.core;

public class Main {

    public static void main(String[] args) {
        int a = 100;
        int b = 2;
        int c = a * b;

        System.out.println(c);

        double alpha = 4.5;
        int y = 7;
        double result = alpha * y;

        String username = "Valentin Markov";
        String city = "Sofia";

        System.out.println(username);
        System.out.println(city);
        System.out.println(result);
    }
}
